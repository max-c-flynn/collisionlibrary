// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2022 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_LIBRARYASSIGN
#define COLLISIONLIBRARY_LIBRARYASSIGN
#include "collisionlibrary"


void lib::Library::assign_fracs_by_names(std::vector<std::string> names, std::vector<double> fracs) {

	if (names.size() != fracs.size()) {
		lib::unequal_pairs_err();
	}


	for (int i = 0; i < names.size(); ++i) {
		assign_fracs_by_names(names[i], fracs[i]);
	}



}

void lib::Library::assign_fracs_by_names(std::vector<std::pair<std::string, double>> pairs) {

	for (auto it = pairs.begin(); it != pairs.end(); ++it) {
		assign_fracs_by_names(it->first, it->second);
	}

}

void lib::Library::assign_fracs_by_names(std::string name, double frac) {

	lib::debug_statement("Assigning fraction [" + std::to_string(frac) + "] to species with name [" + name + "].");

	(*get_species(name))._frac = frac;

}

// to assign frac to nth species regardless of type
void lib::Library::assign_frac_by_index(int k, double frac) {

	lib::debug_statement("Assigning fraction [" + std::to_string(frac) + "] to species at index [" + std::to_string(k) + "].");


	if (k > n_all()) {
		lib::bad_index(k);
	}

	std::string temp_name = allspecies.at(k)->_name;
	assign_fracs_by_names(temp_name, frac);
}

// Assign every collision of a certain type the same style of scattering (for all species)
void lib::Library::assign_scattering_by_type(lib::Scattering scattering, lib::CollisionCode c) {

	lib::debug_statement("Assigning scattering-type [" + scattering.name + "] to all collisions of type [" + lib::collision_code_str_map.find(c)->second + 
		"] for all species.");

	for (auto& spec : allspecies) {
	
		auto ptr = spec->pick_from_code(c);

		for (auto x : *ptr) {

			x->scattering(scattering);

		}
	}
}


// code-friendly type useful for multibolt-command
void lib::Library::assign_scattering_by_type(lib::ScatteringCode s, lib::CollisionCode c, double screen_eV=lib::HARTREE) {

	lib::Scattering scattering;

	switch (s) {
	case lib::ScatteringCode::Isotropic :
		scattering = lib::isotropic_scattering();
		break;
	case lib::ScatteringCode::IdealForward :
		scattering = lib::forward_scattering();
		break;
	case lib::ScatteringCode::ScreenedCoulomb :
		scattering = lib::screened_coulomb_scattering(screen_eV);
		break;
	}

	lib::debug_statement("Assigning scattering-type [" + scattering.name + "] to all collisions of type [" + lib::collision_code_str_map.find(c)->second +
		"] for all species.");

	for (auto& spec : allspecies) {

		auto ptr = spec->pick_from_code(c);

		for (auto x : *ptr) {

			x->scattering(scattering);
		}
	}


		

}

#endif