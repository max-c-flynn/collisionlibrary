// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_SPECIESASSIGN
#define COLLISIONLIBRARY_SPECIESASSIGN
#include "collisionlibrary"

void lib::Species::assign_scale_by_process(std::string process, double s) {
	for (auto it = allcollisions.begin(); it != allcollisions.end(); ++it) {
		if ((*it)->process().compare(process) == 0) {


			lib::debug_statement("Assigned scale [" + std::to_string(s) + "] to Xsec with process [" + process + "]");
			(*it)->scale(s);

		}
	}
}

// to assign nth collision regardless of type
void lib::Species::assign_scale_by_index(int k, double s) {

	if (k > n_all()) {
		lib::bad_index(k);
		return;
	}

	std::string temp_process = allcollisions.at(k)->process();
	assign_scale_by_process(temp_process, s);
}

// case for when the collision-type is known
void lib::Species::assign_scale_by_index(lib::CollisionCode c, int k, double s) {

	XsecPtrVec* ptr = pick_from_code(c);

	if (k > ptr->size()) {
		lib::bad_index(k);
		return;
	}

	std::string temp_process = (*ptr).at(k)->process();
	assign_scale_by_process(temp_process, s);
}
#endif